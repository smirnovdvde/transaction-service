package com.example.transactionservice.model.events;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.Objects;
import java.util.UUID;

public final class TransactionEvent {

    public final UUID id;
    public final ZonedDateTime timestamp;
    public final BigDecimal amount;
    public final boolean reverted;

    public TransactionEvent(UUID id, ZonedDateTime timestamp, BigDecimal amount, boolean reverted) {
        this.id = id;
        this.timestamp = timestamp;
        this.amount = amount;
        this.reverted = reverted;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TransactionEvent that = (TransactionEvent) o;
        return reverted == that.reverted && Objects.equals(id, that.id) && Objects.equals(timestamp, that.timestamp) && Objects.equals(amount, that.amount);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, timestamp, amount, reverted);
    }
}

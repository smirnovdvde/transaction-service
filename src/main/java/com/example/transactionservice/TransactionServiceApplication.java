package com.example.transactionservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.transaction.KafkaTransactionManager;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.web.client.RestTemplate;

import javax.persistence.EntityManagerFactory;
import java.util.UUID;

@SpringBootApplication
@EnableConfigurationProperties
public class TransactionServiceApplication {
    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder restTemplateBuilder) {
        return restTemplateBuilder.build();
    }

    @Bean
    @Primary
    public JpaTransactionManager transactionManager(@Autowired EntityManagerFactory entityManagerFactory) {
        return new JpaTransactionManager(entityManagerFactory);
    }
    @Bean
    public KafkaTransactionManager<UUID, Object> kafkaTransactionManager(@Autowired ProducerFactory<UUID, Object> producerFactory) {
        return new KafkaTransactionManager<>(producerFactory);
    }
    @Bean
    public KafkaTemplate<UUID, Object> kafkaTemplate(@Autowired ProducerFactory<UUID, Object> producerFactory) {
        return new KafkaTemplate<>(producerFactory);
    }

    public static void main(String[] args) {
        SpringApplication.run(TransactionServiceApplication.class, args);
    }
}
